# Online Pizza Order

## Requirement:  
    • In this system, while accessing for the first time, customers needs to register themselves by filling up basic registration details.  
    • Once the registration is successful, customer need to login with a valid username and password for secure login.  
    • After successful log in, customer will see the homepage where various types of pizzas will be listed with their image.  
    • The menu will be visible to the customer with the pizzas. All the ingredients will be shown with their prices.   
    • After selecting a desired pizza, customers can view the details of pizza such as price, category and toppings.  
    • Customer can specify the extra toppings (if required) and enter the quantity required and add a pizza to cart.  
    • Customer can directly click on buy now to place an order.  
    • Now payment option is shown to the customer. He has to choose from the various online payment methods or cash on delivery option.   
    • Once the order is placed and order id will be provided to the customer using which he/she can track the ordered pizza.  
    • Customer can notify the admin about the system by writing a feedback message.  
    • Customers can change their old password with new one whenever required.  
    • Admin can add various pizzas into the system, also can view/edit/delete the added pizza.  
    • All the orders will be displayed to the admin where admin can update the order details.  
    • Admin can view the pizza sales details based on any two selected dates.  
    • Also can view feedback messages received from the registered customers.   
    
    
   ## Modules:
The system comprises of 2 major modules with their sub-modules as follows:  
   ### 1. Admin:  
    a. Add Pizza: Add different types of pizza’s in veg and non-veg category.
    b. View/Edit/Delete: Can view/update/delete the added pizza from the database.
    c. View/Update Order: Can view all the orders received from the customer and change the order details accordingly.
    d. View Sales: Can view sales details based on any two between dates.
    e. View Feedback: Admin can see user’s feedback.  
   ### 2. Customer:
    a. Registration: User can register his detail.
    b. Login: User Login his account.
    c. Home page: User can visit his home page.
    d. Pizza Detail: Can view pizza details by selecting a pizza and view its details such as price, toppings, etc…
        ◦ Add to Cart: Customer can add the selected pizza into cart and can check out further.
        ◦ Buy Now: Can buy a selected pizza and can also enter required toppings (If needed) and specify the quantity.
    e. Review Order    
    f. Replaced Order    
    g. Check Order Status: Can track the ordered pizza by order id to know the order status.
    h. Write Feedback: User can write his review as feedback.
    i. Change Password: User can change his current password and make new password.
   ### 3. Restaurant Employee
    a.Registration :Restaurant register along  detail 
    b.Login : Restaurant login there account
    c.Add New Items : Add New items as per item detail
    d.Review Order
    e.Order Receive
    f.Update Order Status : As soon as order is deliver to Customer it will updated 
    g.Update Items Details: Here they can update(Price,Image,description)
    h.Receive Confirmation
    i.Check Out(Log Out)
